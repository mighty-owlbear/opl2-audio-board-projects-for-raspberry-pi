#include <ncurses.h> /* remember to sudo apt-get install libncurses5-dev libncursesw5-dev */
#include <string.h> 
#include <OPL2.h>
#include <stdio.h>
#include <wiringPi.h>
#include "midi_instruments.h"

using namespace std;

OPL2 opl2;

int kbhit()
{
    int ch = getch();
    if (ch != ERR) {
        ungetch(ch);
        return 1;
    } else {
        return 0;
    }
}

int main(int argc, char **argv) {
    opl2.begin();
    
    // Create an instrument
    Instrument myInstrument = opl2.createInstrument();
    myInstrument.operators[OPERATOR1].frequencyMultiplier = 1;
    myInstrument.operators[OPERATOR1].keyScaleLevel = 1;
    myInstrument.operators[OPERATOR1].outputLevel = 15;
    myInstrument.operators[OPERATOR1].attack = 15;
    myInstrument.operators[OPERATOR1].decay = 1;
    myInstrument.operators[OPERATOR1].sustain = 5;

    myInstrument.operators[OPERATOR2].keyScaleLevel = 1;
    myInstrument.operators[OPERATOR2].outputLevel = 4;
    myInstrument.operators[OPERATOR2].attack = 13;
    myInstrument.operators[OPERATOR2].decay = 2;
    myInstrument.operators[OPERATOR2].sustain = 7;
    myInstrument.operators[OPERATOR2].release = 0;

    myInstrument.feedback = 3;

    // Load instrument
    opl2.setInstrument(0, myInstrument, 0.8);
    
    initscr();
    cbreak();
    noecho();
    nodelay(stdscr, TRUE);
    
    scrollok(stdscr, TRUE);
    while (1) {
        if (kbhit()) {
	    int key = getch();
	    if (key==49){
		opl2.playNote(0, 5, NOTE_C);    // Play note C-5 on channel 0.
		printw("C5\n");
	    } else if(key==50){
		opl2.playNote(0, 5, NOTE_D);
		printw("D5\n");
	    } else if(key==51){
		opl2.playNote(0, 5, NOTE_E);
		printw("E5\n");
	    } else if(key==52){
		opl2.playNote(0, 5, NOTE_F);
		printw("F5\n");
	    } else if(key==53){
		opl2.playNote(0, 5, NOTE_G);
		printw("G5\n");
	    } else if(key==54){
		opl2.playNote(0, 5, NOTE_A);
		printw("A5\n");
	    } else if(key==55){
		opl2.playNote(0, 5, NOTE_B);
		printw("B5\n");
	    } else if(key==56){
		opl2.playNote(0, 6, NOTE_C);
		printw("C6\n");
	    } else if(key==57){
		opl2.playNote(0, 6, NOTE_D);
		printw("D6\n");
	    } else if (key==48){
		opl2.playNote(0, 6, NOTE_E);
		printw("E6\n");
	    }
	    delay(100);                     // Hold the note for 500ms.
            opl2.setKeyOn(0, false);        // Release the note.
        }
    }
}


