#include <ncurses.h> /* remember to sudo apt-get install libncurses5-dev libncursesw5-dev */
#include <OPL2.h>
#include <stdio.h>
#include <wiringPi.h>
#include "midi_instruments.h"

OPL2 opl2;

int kbhit()
{
    int ch = getch();
    if (ch != ERR) {
        ungetch(ch);
        return 1;
    } else {
        return 0;
    }
}

int main(int argc, char **argv) {
    opl2.begin();
		
    // Load a harpsichord instrument and assign it to channel 0.
    Instrument harpsichord = opl2.loadInstrument(INSTRUMENT_HARPSIC);
    opl2.setInstrument(0, harpsichord);
    
    initscr();
    cbreak();
    scrollok(stdscr, TRUE);
    while (1) {
        if (kbhit()) {
	    int key = getch();
	    if (key==49){
		opl2.playNote(0, 5, NOTE_C);    // Play note C-5 on channel 0
	    } else if(key==50){
		opl2.playNote(0, 5, NOTE_D);
	    } else if(key==51){
		opl2.playNote(0, 5, NOTE_E);
	    } else if(key==52){
		opl2.playNote(0, 5, NOTE_F);
	    } else if(key==53){
		opl2.playNote(0, 5, NOTE_G);
	    } else if(key==54){
		opl2.playNote(0, 5, NOTE_A);
	    } else if(key==55){
		opl2.playNote(0, 5, NOTE_B);
	    } else if(key==56){
		opl2.playNote(0, 6, NOTE_C);
	    } else if(key==57){
		opl2.playNote(0, 6, NOTE_D);
	    } else if (key==48){
		opl2.playNote(0, 6, NOTE_E);
	    }
	    delay(500);                     // Hold the note for 500ms.
        opl2.setKeyOn(0, false);        // Release the note.
        }
    }
}
