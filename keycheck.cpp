#include <ncurses.h> 
#include <string.h> 
#include <stdio.h>
#include <unistd.h>  /* for sleep() */
/* build with gcc -std=c++11 -Wall -o keycheck keycheck.cpp -lncurses */
/* don't forget to sudo apt-get install libncurses5-dev libncursesw5-dev */
/* signficant credit to this stackoverflow user from the distant past https://stackoverflow.com/a/4028974 */

int kbhit()
{
    int ch = getch();

    if (ch != ERR) {
        ungetch(ch);
        return 1;
    } else {
        return 0;
    }
}


int main(void)
{
    initscr();
    printw("Press any key to display keycode.\n");
    cbreak();
    nodelay(stdscr, TRUE);

    scrollok(stdscr, TRUE);
    while (1) {
        if (kbhit()) {
            printw(" key character number is %d\n", getch());
            refresh();
        }
    }
}


