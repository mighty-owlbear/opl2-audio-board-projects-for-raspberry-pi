# OPL2 Audio Board projects for Raspberry Pi

Simple projects and helper code using the Cheerful Electronic OPL2 Audio Board to support the Retro Noises tutorial (The MagPi 110).

About the OPL2 audio board: https://cheerful.nl/OPL2_Audio_Board/index.html

Buy the hardware: https://www.tindie.com/products/cheerful/opl2-audio-board/

Software libraries: https://github.com/DhrBaksteen/ArduinoOPL2


----
Anti-deletion line, please ignore.
